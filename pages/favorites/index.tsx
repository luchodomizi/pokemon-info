import React, { useEffect, useState } from "react";
import { Layout } from "../../components/layouts";
import { NoFavourites, Favourites } from "../../components/ui";
import { pokemons } from "../../utils";

const Favoritos = () => {
  const [favouritePokemons, setFavouritePokemons] = useState<number[]>([]);

  useEffect(() => {
    setFavouritePokemons(pokemons());
  }, []);

  return (
    <Layout title="Pokemons Favoritos">
      {favouritePokemons.length === 0 ? (
        <NoFavourites />
      ) : (
        <Favourites favouritePokemons={favouritePokemons} />
      )}
    </Layout>
  );
};

export default Favoritos;
