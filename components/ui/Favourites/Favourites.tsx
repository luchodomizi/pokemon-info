import { Grid } from "@nextui-org/react";
import React, { FC } from "react";
import { FavouritePokemonCard } from "./FavouritePokemonCard";

interface Props {
  favouritePokemons: number[];
}

export const Favourites: FC<Props> = ({ favouritePokemons }) => {
  return (
    <Grid.Container gap={2} direction="row" justify="flex-start">
      {favouritePokemons.map((id) => (
        <FavouritePokemonCard id={id} key={id} />
      ))}
    </Grid.Container>
  );
};
