import Head from "next/head";
import { Navbar } from "../ui";

type Props = {
  children?: React.ReactNode;
  title?: string;
};

const origin = typeof window === "undefined" ? "" : window.location.origin;

export const Layout: React.FC<Props> = ({ children, title }) => {
  return (
    <>
      <Head>
        <title>{title || "Pokemon App"}</title>
        <meta name="author" content="Humberto Domizi" />
        <meta
          name="description"
          content={`Información sobre el Pokemon: ${title}`}
        />
        <meta name="keyword" content={`${title}, pokedex, pokemon`} />
        <meta property="og:title" content={title} />
        <meta property="og:description" content={`Ésto es: ${title}`} />
        <meta property="og:image" content={`${origin}/assets/img/banner.png`} />
      </Head>

      <Navbar />

      <main
        style={{
          padding: "0px 20px",
        }}
      >
        {children}
      </main>
    </>
  );
};
